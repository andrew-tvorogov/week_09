package ru.edu.task3.xml;

/**
 * ReadOnly
 */
public class DebugDependency implements DependencyObject {
    @Override
    public String getValue() {
        return "DEBUG";
    }
}
