package ru.edu.task1.java;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
public class ComponentB {

    private String string;

    public ComponentB(String string) {
        this.string = string;
    }

    public boolean isValid() {
        return "stringForComponentB".equals(string);
    }
}
