package ru.edu.task1.java;

/**
 * ReadOnly. Можно только добавлять аннотации.
 */
public class ComponentC {

    private boolean isInit;


    public void init() {
        isInit = true;
    }


    public boolean isValid() {
        return isInit;
    }
}
